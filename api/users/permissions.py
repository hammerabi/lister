from rest_framework import permissions


class IsCorrectUser(permissions.BasePermission):
    """
    Custom permission to only allow users belonging to a list to see it or edit it.
    """

    def has_object_permission(self, request, view, obj):

        # Write permissions are only allowed to the owner of the snippet.
        return request.user == obj or request.user in obj.friends.all()