from rest_framework import mixins, permissions
from rest_framework.viewsets import GenericViewSet
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .serializers import UserSerializer
from .models import ListerUser
from .permissions import IsCorrectUser


class UserViewSet(mixins.ListModelMixin,
                 mixins.CreateModelMixin,
                 mixins.RetrieveModelMixin,
                 GenericViewSet):
    """
    The following endpoints are fully provided by mixins:
    * List view
    * Create view
    """
    permission_classes = [IsAuthenticated, IsCorrectUser,]
    queryset = ListerUser.objects.all()
    serializer_class = UserSerializer
