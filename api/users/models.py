from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

from lists.models import List

from .managers import ListerUserManager


class ListerUser(AbstractUser):
    username = None
    email = models.EmailField(_("email address"), unique=True)
    lists = models.ManyToManyField(List)
    friends = models.ManyToManyField(
        'self',
        through='Friendship',
        symmetrical=False,
    )

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = ListerUserManager()

    def __str__(self):
        return self.email

class Friendship(models.Model):
    from_user = models.ForeignKey(ListerUser, related_name="from_user", on_delete=models.PROTECT)
    to_user = models.ForeignKey(ListerUser, related_name="to_user", on_delete=models.PROTECT)