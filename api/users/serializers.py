from .models import ListerUser
from rest_framework import serializers
from django.contrib.auth.hashers import make_password

from lists.serializers import ListSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListerUser
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'is_active',
            'id',
            'password',
            'lists',
        )

    password = serializers.CharField(
        write_only=True,
        required=True,
        help_text='Leave empty if no change needed',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )
    first_name = serializers.CharField(
        required=True
    )
    last_name = serializers.CharField(
        required=True
    )
    lists = ListSerializer(many=True, required=False)

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        return super(UserSerializer, self).create(validated_data)