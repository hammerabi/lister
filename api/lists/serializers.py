from .models import List, ListItem
from rest_framework import serializers
from django.contrib.auth.hashers import make_password


class ListItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListItem
        fields = ('id', 'text', 'satisfied',)

class ListSerializer(serializers.ModelSerializer):
    class Meta:
        model = List
        fields = ('id', 'title', 'items',)

    items = ListItemSerializer(many=True)

