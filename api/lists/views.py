from django.shortcuts import render

# Create your views here.

from rest_framework import mixins, permissions
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated

from .serializers import ListSerializer, ListItemSerializer
from .models import List, ListItem
from .permissions import IsListOwner


class ListViewSet(mixins.ListModelMixin,
                 mixins.CreateModelMixin,
                 mixins.RetrieveModelMixin,
                 mixins.UpdateModelMixin,
                 GenericViewSet):
    """
    The following endpoints are fully provided by mixins:
    * List view
    * Create view
    """
    permission_classes = [IsAuthenticated, IsListOwner,]
    queryset = List.objects.all()
    serializer_class = ListSerializer

class ListItemViewSet(mixins.ListModelMixin,
                 mixins.CreateModelMixin,
                 mixins.RetrieveModelMixin,
                 mixins.UpdateModelMixin,
                 GenericViewSet):
    """
    The following endpoints are fully provided by mixins:
    * List view
    * Create view
    """
    permission_classes = [IsAuthenticated,]
    queryset = ListItem.objects.all()
    serializer_class = ListItemSerializer
