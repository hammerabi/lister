from django.db import models


class ListItem(models.Model):
    text = models.CharField(max_length=255)
    satisfied = models.BooleanField(default=False)

    def __str__(self):
        return self.text

class List(models.Model):
    title = models.CharField(max_length=255)
    items = models.ManyToManyField(ListItem)
    users = models.ManyToManyField('users.ListerUser')

    def __str__(self):
        return self.title
