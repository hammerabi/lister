from rest_framework import permissions


class IsListOwner(permissions.BasePermission):
    """
    Custom permission to only allow users belonging to a list to see it or edit it.
    """

    def has_object_permission(self, request, view, obj):

        # Write permissions are only allowed to the owner of the snippet.
        return request.user in obj.users.all()