from django.urls import include, path
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_simplejwt import views as jwt_views


from users.views import UserViewSet
from lists.views import ListViewSet, ListItemViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'lists', ListViewSet)
router.register(r'list-items', ListItemViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]