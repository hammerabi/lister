FROM python:3.7.4-alpine3.10

WORKDIR /src/api

# System setup
RUN apk update
RUN apk add build-base gcc abuild binutils binutils-doc gcc-doc
RUN apk add python-dev python3-dev
RUN apk add mariadb-dev
COPY . /src
EXPOSE 8000

# Python setup
ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip
RUN pip3 install pipenv
RUN pipenv install --system --deploy

# DB dependency setup          
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "wsgi:application"]